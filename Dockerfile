FROM python:3.8.2-alpine AS build-python


WORKDIR /usr/src/app
FROM python:3.8.2-alpine
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev \
    && pip install psycopg2 \
    && apk del build-deps

RUN pip install --upgrade pip
RUN pip install pipenv 
COPY ./Pipfile Pipfile.lock /
# RUN pipenv install
RUN pipenv install --skip-lock --system
WORKDIR /usr/src/app

COPY . .

RUN python manage.py collectstatic --noinput
RUN adduser -D myuser
USER myuser
CMD gunicorn hello_django.wsgi:application --bind 0.0.0.0:$PORT